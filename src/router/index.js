import { createRouter, createWebHistory } from "vue-router";
import BuySell from "../components/BuySell.vue";
import CryptoBasics from "@/components/CryptoBasics";
import Support from "@/components/Support";
import Home from "@/views/Home";
import TermsOfService from "@/components/TermsOfService";
import AmlAndPrivacyPolicy from "@/components/AmlAndPrivacyPolicy";

const routes = [
	{
		path: "/home",
		name: "Home",
		component: Home,
	},
  {
    path: "/buy-sell",
    name: "BuySell",
    component: BuySell,
  },
	{
		path: "/crypto-basics",
		name: "CryptoBasics",
		component: CryptoBasics,
	},
	{
		path: "/support",
		name: "Support",
		component: Support,
	},
	{
		path: "/terms-of-service",
		name: "TermsOfService",
		component: TermsOfService,
	},
	{
		path: "/aml-and-privacy-policy",
		name: "AmlAndPrivacyPolicy",
		component: AmlAndPrivacyPolicy,
	},
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../components/About.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
